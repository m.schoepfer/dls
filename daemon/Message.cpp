/*****************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <sstream>
#include <iostream>

#include <pdcom5/Variable.h>

#include "Message.h"
#include "MessageList.h"
#include "globals.h"

/****************************************************************************/

/** Constructor.
 */
Message::Message(
        MessageList *list,
        xmlNode *node
        ):
    BaseMessage(node),
    PdCom::Subscriber(PdCom::event_mode),
    _parent_list(list),
    _value(0.0),
    _data_present(false)
{
}

/****************************************************************************/

/** Destructor.
 */
Message::~Message()
{
}

/****************************************************************************/

/** Subscribe variable.
 */
void Message::subscribe(PdCom::Process *process)
{
    unsubscribe();

#if 0
    msg() << __func__ << "() " << path();
    log(::Info);
#endif

    _data_present = false;

    try {
        PdCom::Selector selector;
        if (index() != -1)
            selector = PdCom::ScalarSelector({index()});

        _subscription = PdCom::Subscription(*this, *process, path(), selector);
    }
    catch (PdCom::Exception &e) {
        msg() << "Message variable subscription failed!";
        log(::Error);
    }
}

/****************************************************************************/

void Message::stateChanged(const PdCom::Subscription&)
{
    if (_subscription.getState() == PdCom::Subscription::State::Active) {
        try {
            _subscription.poll();
        }
        catch (PdCom::Exception &e) {
            msg() << "Message poll failed!";
            log(::Error);
            unsubscribe();
        }
    }
    else if (_subscription.getState() == PdCom::Subscription::State::Invalid) {
        msg() << "Message variable subscription failed!";
        log(::Error);
        unsubscribe();
    }
}

/****************************************************************************/

/** Unsubscribe from variable.
 */
void Message::unsubscribe()
{
    _subscription = {};
    _data_present = false;
}

/****************************************************************************/

void Message::newValues(std::chrono::nanoseconds ts)
{

    double new_value;
    _subscription.getValue(new_value);

    if (_data_present && _value == 0.0 && new_value != 0.0) {
        const LibDLS::Time time (ts);
        std::string storeType;

        switch (type()) {
            case Information:
                storeType = "info";
                break;
            case Warning:
                storeType = "warn";
                break;
            case Error:
                storeType = "error";
                break;
            case Critical:
                storeType = "error";
                break;
            default:
                storeType = "info";
                break;
        }

        _parent_list->store_message(time, storeType, path(), index());
    }

    _value = new_value;
    _data_present = true;
}

/****************************************************************************/
