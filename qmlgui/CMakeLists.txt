#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.4)

project(QmlGui)

find_package(DlsWidgets REQUIRED)
find_package(Qt5 REQUIRED COMPONENTS Core Gui Svg Widgets Xml QuickWidgets Qml)
include(GNUInstallDirs)

add_executable(${PROJECT_NAME}
    main.cpp
    dlswidget.cpp
    dlswidget.h
    qmlgui.qrc
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    AUTOMOC ON
    AUTORCC ON
)

target_link_libraries(${PROJECT_NAME} PUBLIC
    EtherLab::DlsWidgets
    Qt5::Gui Qt5::Widgets Qt5::Core Qt5::Xml Qt5::Svg Qt5::QuickWidgets Qt5::Qml
)

install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
)
