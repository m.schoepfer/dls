/*****************************************************************************
 *
 * Copyright (C) 2021  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the DLS widget library.
 *
 * The DLS widget library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The DLS widget library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the DLS widget library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 *
 ***************************************************************************/


#ifndef EMBEDDED_DLSWIDGET_H
#define EMBEDDED_DLSWIDGET_H

#include <QWidget>
#include <QString>
#include <QEvent>
#include <QSize>
#include <QMouseEvent>
#include <QPixmap>
#include <QtQuick/QQuickPaintedItem>
#include <QAction>

#include "DlsWidgets/Model.h"
#include "DlsWidgets/Graph.h"

class DlsWidget : public QQuickPaintedItem 
{
  Q_OBJECT
public:
    DlsWidget(QQuickItem *parent = 0); 
    void paint(QPainter *painter);

public slots:
  void actionTrigger(QString); 
  void dlsGraphUpdated();

private:
    QtDls::Model dlsModel;
    DLS::Graph dlsGraph;
 protected:
    bool event(QEvent *event);
    bool eventFilter(QObject *obj, QEvent *event) override;
};

#endif
