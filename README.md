# What is DLS?

The [Data Logging Service (DLS)](https://etherlab.org/en/dls) is a data logging
system for [EtherLab](https://etherlab.org/en), that is capable of collecting,
compressing and storing high-frequency realtime data.  The goal is, to allow
the user unlimited and performant access to the stored data. A signal overview
over a year is retrieved as fast as a tiny signal fluctuation, that happened in
a fraction of a second.

# Documentation

Documentation is available in
[German](https://gitlab.com/etherlab.org/dls/-/jobs/artifacts/master/raw/pdf/dls_doku.pdf?job=pdf)
and
[English](https://gitlab.com/etherlab.org/dls/-/jobs/artifacts/master/raw/pdf/dls_doku_en.pdf?job=pdf)
language.
