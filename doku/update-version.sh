#!/bin/sh
#
# Output information about parent revision to TeX-includable file.
#

unset GIT_DIR

set -e

if [ "$1" != "--no-cd" ]; then
	cd doku
fi

REV=$(git describe --tags)
# 1.4.0-rc2-179-gcf2f735
DATE=$(git show -s --format=%cd --date=short)
# 2021-04-14

OUT=version.tex
cat - > $OUT <<EOF
\\def\\revision{$REV}
\\def\\isodate#1-#2-#3x{
  \\day  = #3
  \\month= #2
  \\year = #1
}
\\isodate ${DATE}x
EOF


echo Updated $OUT
