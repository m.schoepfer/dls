/*****************************************************************************
 *
 * Copyright (C) 2009 - 2017  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the DLS widget library.
 *
 * The DLS widget library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The DLS widget library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the DLS widget library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef DLSWIDGET_EXPORT_H
#define DLSWIDGET_EXPORT_H

#include <QtCore/QtGlobal>


#ifdef DlsWidgets_EXPORTS

#ifndef DLS_NO_DESIGNER
#  if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
#    include <QtUiPlugin/QDesignerExportWidget>
#  else
#    include <QtDesigner/QDesignerExportWidget>
#  endif // QT_VERSION
#  define DLSWIDGETS_PUBLIC QDESIGNER_WIDGET_EXPORT
#else
#  define DLSWIDGETS_PUBLIC Q_DECL_EXPORT
#endif
#else
# define DLSWIDGETS_PUBLIC Q_DECL_IMPORT
#endif  // DlsWidgets_EXPORTS

#endif  // DLSWIDGET_EXPORT_H
