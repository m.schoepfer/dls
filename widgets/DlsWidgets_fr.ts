<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>DLS::ExportDialog</name>
    <message>
        <location filename="ExportDialog.cpp" line="110"/>
        <source>Failed to create export directory %1.</source>
        <translation>Impossible de créer le dossier d&apos;exportation %1.</translation>
    </message>
    <message>
        <location filename="ExportDialog.cpp" line="122"/>
        <source>Failed to open %1.</source>
        <translation>Impossible d&apos;ouvrir %1.</translation>
    </message>
    <message>
        <location filename="ExportDialog.cpp" line="263"/>
        <source>Target Directory</source>
        <translation>Dossier cible</translation>
    </message>
</context>
<context>
    <name>DLS::FilterDialog</name>
    <message>
        <location filename="FilterDialog.cpp" line="83"/>
        <source>At %1: %2</source>
        <translation>A l&apos;emplacement %1: %2</translation>
    </message>
    <message>
        <location filename="FilterDialog.cpp" line="100"/>
        <source>Pattern valid.</source>
        <translation>Le modèle est valide.</translation>
    </message>
</context>
<context>
    <name>DLS::Graph</name>
    <message>
        <location filename="Graph.cpp" line="1721"/>
        <source>&amp;Fix measuring line</source>
        <translation>&amp;Attacher la barre de mesure</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1723"/>
        <source>Fix the measuring line at the current time.</source>
        <translation>Attacher la barre de mesure au temps courant.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1725"/>
        <source>&amp;Remove measuring line</source>
        <translation>Supprimer la barre de m&amp;esure</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1726"/>
        <source>Remove the measuring line.</source>
        <translation>Supprimer la barre de mesure.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1728"/>
        <source>&amp;Previous view</source>
        <translation>&amp;Vue précédente</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1729"/>
        <source>Return to previous view.</source>
        <translation>Retourner à la vue précédente.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1731"/>
        <source>&amp;Next view</source>
        <translation>Vue suiva&amp;nte</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1732"/>
        <source>Proceed to next view.</source>
        <translation>Passer à la vue suivante.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1734"/>
        <source>&amp;Update</source>
        <translation>&amp;Réactualiser</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1735"/>
        <source>Update displayed data.</source>
        <translation>Réactualiser les données affichées.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1737"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Zoom</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1738"/>
        <source>Set mouse interaction to zooming.</source>
        <translation>Définir les interactions de la souris pour le zoom.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1740"/>
        <source>&amp;Pan</source>
        <translation>&amp;Intervalle</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1741"/>
        <source>Set mouse interaction to panning.</source>
        <translation>Définir les interactions de la souris pour l&apos;intervalle.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1743"/>
        <source>&amp;Measure</source>
        <translation>&amp;Mesure</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1744"/>
        <source>Set mouse interaction to measuring.</source>
        <translation>Définir les interactions de la souris pour la mesure.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1746"/>
        <source>Zoom in</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1747"/>
        <source>Zoom the current view in to half of the time around the center.</source>
        <translation>Agrandir la vue actuelle jusqu&apos;`a la moitié du temps autour du centre.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1750"/>
        <source>Zoom out</source>
        <translation>Réduire</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1751"/>
        <source>Zoom the current view out the double time around the center.</source>
        <translation>Agrandir la vue actuelle sur le double du temps autour du centre.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1754"/>
        <source>Auto range</source>
        <translation>Intervalle automatique</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1756"/>
        <source>Automatically zoom to the data extent.</source>
        <translation>Zoom automatique sur l&apos;étendue de données.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1760"/>
        <source>Choose date...</source>
        <translation>Choisir une date...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1762"/>
        <source>Open a dialog for date picking.</source>
        <translation>Ouvrir une boîte de dialogue pour choisir une date.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1764"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1766"/>
        <source>Set the date range to today.</source>
        <translation>Configurer l&apos;intervalle de temps pour aujourd&apos;hui.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1768"/>
        <source>Yesterday</source>
        <translation>Hier</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1770"/>
        <source>Set the date range to yesterday.</source>
        <translation>Configurer l&apos;intervalle de temps pour hier.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1772"/>
        <source>This week</source>
        <translation>Semaine actuelle</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1774"/>
        <source>Set the date range to this week.</source>
        <translation>Configurer l&apos;intervalle de temps pour cette semaine.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1776"/>
        <source>Last week</source>
        <translation>Semaine précédente</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1778"/>
        <source>Set the date range to last week.</source>
        <translation>Configurer l&apos;intervalle de temps pour la semaine précédente.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1780"/>
        <source>This month</source>
        <translation>Mois actuel</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1782"/>
        <source>Set the date range to this month.</source>
        <translation>Configurer l&apos;intervalle de temps pour le mois actuel.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1784"/>
        <source>Last month</source>
        <translation>Mois précédent</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1786"/>
        <source>Set the date range to last month.</source>
        <translation>Configurer l&apos;intervalle de temps pour le mois précedent.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1788"/>
        <source>This year</source>
        <translation>Année actuelle</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1790"/>
        <source>Set the date range to this year.</source>
        <translation>Configurer l&apos;intervalle de temps pour cette année.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1792"/>
        <source>Last year</source>
        <translation>Année précédente</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1794"/>
        <source>Set the date range to last year.</source>
        <translation>Configurer l&apos;intervalle de temps pour l&apos;année précédente.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1796"/>
        <source>Section properties...</source>
        <translation>Propriétés de la section...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1797"/>
        <source>Open the section configuration dialog.</source>
        <translation>Ouvrir la configuration de la section.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1800"/>
        <source>Remove section</source>
        <translation>Supprimer la section</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1801"/>
        <source>Remove the selected section.</source>
        <translation>Supprimer la section selectionnée.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1803"/>
        <source>Clear sections</source>
        <translation>Effacer les sections</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1804"/>
        <source>Remove all sections.</source>
        <translation>Supprimer toutes les sections.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1806"/>
        <source>Show Messages</source>
        <translation>Afficher les messages</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1807"/>
        <source>Show process messages.</source>
        <translation>Afficher les messages des processes.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1809"/>
        <source>Filter messages...</source>
        <translation>Filtrer les messages...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1810"/>
        <source>Filter messages by regular expressions.</source>
        <translation>Filter les messages avec des expressions régulières.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1812"/>
        <source>Print...</source>
        <translation>Imprimer...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1813"/>
        <source>Open the print dialog.</source>
        <translation>Ouvrir la fenêtre d&apos;impression.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1815"/>
        <source>Export...</source>
        <translation>Exporter...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1816"/>
        <source>Open the export dialog.</source>
        <translation>Ouvrir la fenêtre d&apos;exportation.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="390"/>
        <source>Failed to open %1!</source>
        <translation>Impossible d&apos;ouvrir %1 fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="400"/>
        <source>Failed to parse XML, line %2, column %3: %4</source>
        <translation>Impossible d&apos;analyser XML à la ligne %2, colonne %3: %4</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="490"/>
        <source>Failed to open %1 for writing!</source>
        <translation>Impossible d&apos;écrire %1!</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1758"/>
        <source>Go to date</source>
        <translation>Aller à la date</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1676"/>
        <source>Failed to get channel %1: %2</source>
        <translation>Impossible de trouver le canal %1: %2</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="2115"/>
        <source>Failed to load section: %1</source>
        <translation>Impossible de charger la section: %1</translation>
    </message>
    <message>
        <source>Failed to parse section: %1</source>
        <translation type="obsolete">Impossible d&apos;analyser la section: %1</translation>
    </message>
</context>
<context>
    <name>DLS::SectionDialog</name>
    <message numerus="yes">
        <location filename="SectionDialog.cpp" line="264"/>
        <source>Remove %n layer(s)</source>
        <translation>
            <numerusform>Supprime %n couche</numerusform>
            <numerusform>Supprime %n couches</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DLS::SectionModel</name>
    <message>
        <location filename="SectionModel.cpp" line="184"/>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="187"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="190"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="193"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="196"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="199"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="202"/>
        <source>Precision</source>
        <translation>Précision</translation>
    </message>
</context>
<context>
    <name>DatePickerDialog</name>
    <message>
        <location filename="DatePickerDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Calendrier</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="25"/>
        <source>Day</source>
        <translation>Jour</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="35"/>
        <source>Week</source>
        <translation>Semaine</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="42"/>
        <source>Month</source>
        <translation>Mois</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="49"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
</context>
<context>
    <name>Dir</name>
    <message>
        <location filename="Dir.cpp" line="108"/>
        <source>Local directory %1</source>
        <translation>Dossier local %1</translation>
    </message>
    <message>
        <location filename="Dir.cpp" line="114"/>
        <source>Remote directory %1</source>
        <translation>Dossier distant %1</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="ExportDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Exportation</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="28"/>
        <source>Number of Signals:</source>
        <translation>Nombre de signaux:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="54"/>
        <source>Beginning:</source>
        <translation>Début:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="74"/>
        <source>End:</source>
        <translation>Fin:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="94"/>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="116"/>
        <source>Target Directory</source>
        <translation>Dossier cible</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="135"/>
        <source>Change...</source>
        <translation>Modifier...</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="145"/>
        <source>Export Formats</source>
        <translation>Format d&apos;exporation</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="151"/>
        <source>ASCII (*.dat)</source>
        <translation>ASCII (*.dat)</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="161"/>
        <source>Matlab binary, level 4 (*.mat)</source>
        <translation>Format binaire Matlab, Niveau 4 (*.mat)</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="173"/>
        <source>Decimation:</source>
        <translation>Décimation:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="190"/>
        <source>Trim data to viewed time range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="197"/>
        <source>Make times relative to beginning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterDialog</name>
    <message>
        <location filename="FilterDialog.ui" line="14"/>
        <source>Filter messages</source>
        <translation>Filtrer les messages</translation>
    </message>
    <message>
        <location filename="FilterDialog.ui" line="20"/>
        <source>Regular expression (PCRE syntax):</source>
        <translation>Expression régulière (syntaxe PCRE):</translation>
    </message>
</context>
<context>
    <name>Job</name>
    <message>
        <location filename="Job.cpp" line="104"/>
        <source>Job %1</source>
        <translation>Tâche %1</translation>
    </message>
</context>
<context>
    <name>Layer</name>
    <message>
        <location filename="Layer.cpp" line="104"/>
        <source>Layer element missing url attribute!</source>
        <translation>Attribut URL manquant dans l&apos;élément de couche!</translation>
    </message>
    <message>
        <location filename="Layer.cpp" line="244"/>
        <source>Invalid URL %1!</source>
        <translation>URL invalid &quot;%1&quot;!</translation>
    </message>
    <message>
        <location filename="Layer.cpp" line="253"/>
        <source>Failed to get channel %1: %2</source>
        <translation>Impossible de trouver le canal &quot;%1&quot;: %2</translation>
    </message>
</context>
<context>
    <name>QtDls::Model</name>
    <message>
        <location filename="Model.cpp" line="407"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Model.cpp" line="409"/>
        <source>Alias</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <location filename="Section.cpp" line="190"/>
        <source>Invalid value in ScaleMinimum</source>
        <translation>Valeur impossible pour l&apos;attribut ScaleMinimum</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="200"/>
        <source>Invalid value in ScaleMaximum</source>
        <translation>Valeur impossible pour l&apos;attribut ScaleMaximum</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="210"/>
        <source>Invalid value in Height</source>
        <translation>Valeur impossible pour l&apos;attribut Height</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="220"/>
        <source>Invalid value in RelativePrintHeight</source>
        <translation>Valeur impossible pour l&apos;attribut RelativePrintHeight</translation>
    </message>
    <message>
        <source>Layer element missing url attribute!</source>
        <translation type="vanished">Attribut URL manquand dans l&apos;élément de couche!</translation>
    </message>
    <message>
        <source>Invalid URL in Layer element!</source>
        <translation type="vanished">URL invalide dans l&apos;élément de couche!</translation>
    </message>
    <message>
        <source>Failed to get channel %1: %2</source>
        <translation type="vanished">Impossible de trouver le canal %1: %2</translation>
    </message>
    <message>
        <source>Failed to get channel %1!</source>
        <translation type="vanished">Impossible de trouver le canal %1!</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="972"/>
        <source>Failed to load layer: %1</source>
        <translation>Impossible de charger la couche: %1</translation>
    </message>
</context>
<context>
    <name>SectionDialog</name>
    <message>
        <location filename="SectionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Propriétés de la section</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="22"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="28"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="35"/>
        <source>Manual</source>
        <translation>Manuelle</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="44"/>
        <source>Maximum:</source>
        <translation>Maximum:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="51"/>
        <source>Minimum:</source>
        <translation>Minimum:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="80"/>
        <source>Guess from Data</source>
        <translation>Deviner depuis les données</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="87"/>
        <source>Show Scale</source>
        <translation>Afficher l&apos;échelle</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="96"/>
        <source>Relative printing
height:</source>
        <translation>Impression en
hauteur relative:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="104"/>
        <source>(unused)</source>
        <translation>(inutilisé)</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="107"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="161"/>
        <source>Preview</source>
        <translation>Prévisualisation</translation>
    </message>
</context>
</TS>
