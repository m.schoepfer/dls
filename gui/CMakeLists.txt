
#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

# copy resource file to build dir and compile translation files
set(TS_FILES dlsgui_de.ts dlsgui_fr.ts)
configure_file(dlsguits.qrc "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
qt5_add_translation(QM_FILES ${TS_FILES})


add_executable(dlsgui
    AboutDialog.cpp
    LogWindow.cpp
    MainWindow.cpp
    SettingsDialog.cpp
    UriDialog.cpp
    main.cpp

    AboutDialog.ui
    LogWindow.ui
    MainWindow.ui
    SettingsDialog.ui
    UriDialog.ui

    AboutDialog.h
    LogWindow.h
    MainWindow.h
    SettingsDialog.h
    UriDialog.h

    dlsgui.qrc
    "${CMAKE_CURRENT_BINARY_DIR}/dlsguits.qrc"

    dlsgui.rc
    ${QM_FILES}
)

set_target_properties(dlsgui PROPERTIES
    AUTOUIC ON
    AUTOMOC ON
    AUTORCC ON
    WIN32_EXECUTABLE ON
)

target_link_libraries(dlsgui PRIVATE
    dls
    DlsWidgets
    Qt5::Svg Qt5::Widgets Qt5::Core Qt5::Xml Qt5::Gui
)

target_compile_options(dlsgui PRIVATE -Wall)

install(TARGETS dlsgui
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
)

if(NOT WIN32)
    install(FILES images/dlsgui.svg images/dlsgui-view.svg
        DESTINATION "${CMAKE_INSTALL_DATADIR}/icons/hicolor/scalable/apps"
    )

    install(FILES dlsgui.desktop DESTINATION "${CMAKE_INSTALL_DATADIR}/applications")

    install(FILES dlsgui-mime.xml DESTINATION "${CMAKE_INSTALL_DATADIR}/mime/packages")
endif()
