#!/bin/bash

docker run -it --rm \
    --volume ${PWD}/..:/git \
    --workdir /git \
    --user $(id -u):$(id -g) \
    registry.gitlab.com/etherlab.org/build-container-factory/mingw \
    gui/build-win64.sh
