#!/bin/sh

# Copy DLL dependencies from MinGW installation

TARGET="$1"

set -e

if ! test -d $TARGET; then
    echo "$TARGET is no directory." >&2
    exit 1
fi

SYS_ROOT=/usr/x86_64-w64-mingw32/sys-root/mingw

mkdir -p $TARGET/platforms
cp $SYS_ROOT/lib/qt5/plugins/platforms/* $TARGET/platforms/

mkdir -p $TARGET/imageformats
cp $SYS_ROOT/lib/qt5/plugins/imageformats/qsvg.dll $TARGET/imageformats/

mkdir -p $TARGET/printsupport
cp $SYS_ROOT/lib/qt5/plugins/printsupport/windowsprintersupport.dll $TARGET/printsupport

mkdir -p $TARGET/styles
cp $SYS_ROOT/lib/qt5/plugins/styles/* $TARGET/styles

cp $SYS_ROOT/bin/Qt5Core.dll $TARGET
cp $SYS_ROOT/bin/Qt5Gui.dll $TARGET
cp $SYS_ROOT/bin/Qt5PrintSupport.dll $TARGET
cp $SYS_ROOT/bin/Qt5Svg.dll $TARGET
cp $SYS_ROOT/bin/Qt5Widgets.dll $TARGET
cp $SYS_ROOT/bin/Qt5Xml.dll $TARGET
cp $SYS_ROOT/bin/iconv.dll $TARGET
cp $SYS_ROOT/bin/libbz2-1.dll $TARGET
cp $SYS_ROOT/bin/libfftw3-3.dll $TARGET
cp $SYS_ROOT/bin/libfreetype-6.dll $TARGET
cp $SYS_ROOT/bin/libgcc_s_seh-1.dll $TARGET
cp $SYS_ROOT/bin/libglib-2.0-0.dll $TARGET
cp $SYS_ROOT/bin/libharfbuzz-0.dll $TARGET
cp $SYS_ROOT/bin/libintl-8.dll $TARGET
cp $SYS_ROOT/bin/libpcre-1.dll $TARGET
cp $SYS_ROOT/bin/libpcre2-16-0.dll $TARGET
cp $SYS_ROOT/bin/libpcre2-8-0.dll $TARGET
cp $SYS_ROOT/bin/libpng16-16.dll $TARGET
cp $SYS_ROOT/bin/libprotobuf-30.dll $TARGET
cp $SYS_ROOT/bin/libssp-0.dll $TARGET
cp $SYS_ROOT/bin/libstdc++-6.dll $TARGET
cp $SYS_ROOT/bin/liburiparser-1.dll $TARGET
cp $SYS_ROOT/bin/libwinpthread-1.dll $TARGET
cp $SYS_ROOT/bin/libxml2-2.dll $TARGET
cp $SYS_ROOT/bin/zlib1.dll $TARGET
