/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef LibDLSBaseMessageListH
#define LibDLSBaseMessageListH

/*****************************************************************************/

#include <libxml/parser.h>

#include <string>
#include <map>
#include <tuple>

#include "LibDLS/Exception.h"

/*****************************************************************************/

namespace LibDLS {

class BaseMessage;

/*****************************************************************************/

/** Message list.
 */
class BaseMessageList
{
public:
    BaseMessageList();
    virtual ~BaseMessageList();

    void clear();

    static std::string path(const std::string &);
    static bool exists(const std::string &);
    void import(const std::string &);
    unsigned int count() const;
    const BaseMessage *findPath(const std::string &, int index) const;

    /** Exception.
     */
    class Exception:
        public LibDLS::Exception
    {
        public:
            Exception(std::string pmsg):
                LibDLS::Exception(pmsg) {};
    };

protected:
    struct Key {
        std::string path_;
        int index_;

        bool operator<(const Key& rhs) const {
            return std::tie(path_, index_) < std::tie(rhs.path_, rhs.index_);
        }
    };
    std::map<Key, BaseMessage *> _messages; /**< Messages. */

    virtual BaseMessage *newMessage(xmlNode *);
};

} // namespace LibDLS

/*****************************************************************************/

#endif
